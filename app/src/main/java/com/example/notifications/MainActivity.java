package com.example.notifications;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.WebSocket;

import static android.media.RingtoneManager.getDefaultUri;


public class MainActivity extends AppCompatActivity  {


    private String mensaje;
    private RecyclerView recyclerView;

    public WebSocket webSocket;
    private  int NotifyId = 0;
    private Vibrator v;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private int maxDatos = 10;
    public static ProgressDialog d = null;
    public static Context ctx = null;
    private  NotificationManager mNotificationManager;
    private int antDatosSize = 0;
    private  NotificationChannel channel;
    private  NotificationCompat.Builder mBuilder;
    private SwipeRefreshLayout swipeContainer;
    ArrayList<Mensaje> men = new ArrayList<Mensaje>();
    String id = "";



    @RequiresApi(api = Build.VERSION_CODES.P)
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_details);
        ctx = MainActivity.this;

        SQLite.delete(Mensaje.class)
                .execute();
        try {
            Log.d("dateFormat", dateDB());
            boolean resultado = new getMensajesList(dateDB(), ctx).execute().get();

            if(resultado) {
                loadAdapter();
            }else{
                Toast.makeText(this,"No hay mensajes",Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {

            e.printStackTrace();
        }

        v = (Vibrator) getApplicationContext().getSystemService(VIBRATOR_SERVICE);
        //createNotificationChannel();
        //instantiateWebSocket();







        Intent intent =   new Intent(this,MyService.class);
        MyService.ctx = getApplicationContext();
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.O)
            this.startForegroundService(intent);
        else
          this.startService(intent);



        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            Handler handler = new Handler();
            @Override
            public void onRefresh() {
                handler.postDelayed(new Runnable() {
                    @Override public void run() {
                        // Stop animation (This will be after 3 seconds)
                        reloadAdapter(handler);
                    }
                }, 2000); // Delay in millis

            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright);
       /* Intent intent = getIntent();

        Bundle bundle = intent.getExtras();
        if (bundle != null) {

            Log.d("TAG1","String" + intent.getExtras());
            guardarMensaje(bundle);
        }
        /*FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("TAG1", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        // Log and toast
                        String msg = "Esto es el token "+token;
                        Log.d("TAG1", msg);
                        FirebaseService service = new FirebaseService();
                        id = android.provider.Settings.System.getString(getContentResolver(), android.provider.Settings.System.ANDROID_ID);
                        new sendRegistrationToServer(token, id, getApplicationContext()).execute();
                        service.ID = id;
                        service.Ctx = getApplicationContext();




                    }
                });
*/
    }

    @Override
    protected void onRestart() {
        super.onRestart();


    }

    private void setNotification(String notificationMessage) {

//**add this line**
        int requestID = (int) System.currentTimeMillis();

        Uri alarmSound = getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


        Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);

//**add this line**
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

//**edit this line to put requestID as requestCode**
        PendingIntent contentIntent = PendingIntent.getActivity(this, requestID,notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.ic_ac_unit_black_24dp)
                .setContentTitle("Partes de frio")
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(notificationMessage))
                .setContentText(notificationMessage).setAutoCancel(true);
        mBuilder.setSound(alarmSound);
        mBuilder.setContentIntent(contentIntent);
        NotificationManagerCompat mNotificationManager = NotificationManagerCompat.from(this);
        mNotificationManager.notify((int) System.currentTimeMillis(), mBuilder.build());

    }
    @Override
    protected void onResume() {
    Intent intent = new Intent();
        Log.d("TAG1", "onResume: " + intent.getExtras());
        super.onResume();


    }

    public void reloadAdapter(Handler handler){
      /*  SQLite.delete(Mensaje.class)
                .execute();
        try {

            boolean resultado = new getMensajesList("",MainActivity.this).execute().get();
            if(resultado){*/
                maxDatos = maxDatos + 10;
                List<Mensaje> mensajes = SQLite.select().
                        from(Mensaje.class).orderBy(Mensaje_Table.id, false).limit(maxDatos).
                        queryList();
                if(mensajes.size() == antDatosSize ){
                    swipeContainer.setRefreshing(false);
                    handler.removeCallbacks(null);
                    Toast.makeText(MainActivity.this, "No hay mas datos", Toast.LENGTH_SHORT).show();
                    maxDatos = maxDatos - 10;
                }
                else{
                    int diferencia = antDatosSize;
                    men.clear();
                    antDatosSize = mensajes.size();
                    for(int i =  (mensajes.size() -1); i >= 0 ; i--) {

                        men.add(new Mensaje(0,mensajes.get(i).getMensaje(),mensajes.get(i).getUsuario(),mensajes.get(i).getDate()));
                    }

                    // specify an adapter (see also next example)
                    mAdapter = new MyAdapter(men);
                    recyclerView.setAdapter(mAdapter);
                    LinearLayoutManager layoutManager = new CustomLayoutManager(getApplicationContext());
                    recyclerView.setLayoutManager(layoutManager);
                    int position = ( maxDatos == mensajes.size() ? 10 : (mensajes.size() - diferencia ));
                    layoutManager.scrollToPositionWithOffset(position, 0);
                    layoutManager.smoothScrollToPosition(recyclerView,null,0);

                    swipeContainer.setRefreshing(false);

                }
/*

            }
*/

        /*} catch (ExecutionException e) {
            e.printStackTrace();

        } catch (InterruptedException e) {
            e.printStackTrace();

        }
*/

    }

    public void loadAdapter(){
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        List<Mensaje> mensajes = SQLite.select().
                from(Mensaje.class).orderBy(Mensaje_Table.id, false).limit(maxDatos).
                queryList();
        antDatosSize = mensajes.size();
        men.clear();

        for(int i =  (mensajes.size() -1); i >= 0 ; i--) {

            men.add(new Mensaje(0,mensajes.get(i).getMensaje(),mensajes.get(i).getUsuario(),mensajes.get(i).getDate()));

        }
        // specify an adapter (see also next example)
        mAdapter = new MyAdapter(men);
        recyclerView.setAdapter(mAdapter);
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView
                .getLayoutManager();
        if (layoutManager != null) {
            layoutManager.scrollToPositionWithOffset((mensajes.size()-1), 0);
        }

    };



   /* private void guardarMensaje(Bundle extras) {
        if(extras.getString("notification_message") != null) {
            Mensaje dato = new Mensaje();
            dato.setMensaje(extras.getString("notification_message"));
            dato.setUsuario(extras.getString("user"));
            dato.setDate(date());
            dato.save();
            Log.e("TAG1", " MENSAJE " + extras.getString("notification_message"));
            Log.e("TAG1", " USUARIO " + extras.getString("user"));
            Log.e("TAG1", " DATE " + date());
        }
    }*/

    private String date(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        Date date = new Date();

        String fecha = dateFormat.format(date);
        return fecha;
    }

    private String dateDB(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();

        String fecha = dateFormat.format(date);
        return fecha;
    }






//
//    public void instantiateWebSocket() {
//
//
//        OkHttpClient client = new OkHttpClient();
//
//
//        //replace x.x.x.x with your machine's IP Address
//        //Request request = new Request.Builder().url("ws://192.168.55.225:3332").build();
//        Request request = new Request.Builder().url("ws://192.168.0.175:3333").build();
//
//        SocketListener socketListener = new SocketListener(this);
//
//
//        this.webSocket = client.newWebSocket(request, socketListener);
//
//
//
//    }
//
//
//
//
//    public class SocketListener extends WebSocketListener {
//
//
//        MainActivity activity;
//
//
//
//        SocketListener(MainActivity activity) {
//            this.activity = activity;
//        }
//
//
//
//        @Override
//        public void onOpen(WebSocket webSocket, Response response) {
//
//            Log.d("conectify", "run: conectado");
//            activity.runOnUiThread(new Runnable() {
//
//                @Override
//                public void run() {
//                    try {
//                        Toast.makeText(activity, "Conexion establecida ", Toast.LENGTH_LONG).show();
//                    }catch (Exception e){
//                        Log.e("tag3", "run: "+e);
//                        Log.d("conectify", "run: conectado");
//                    }
//
//                }
//
//            });
//
//        }
//
//
//        @Override
//        public void onMessage(WebSocket webSocket, final String text) {
//
//            activity.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//
//                    Log.d("tag1", "run: "+text);
//                    JSONObject jsonObject = null;
//                    try {
//                        jsonObject = new JSONObject(text);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                    try {
//
//
//                        if (jsonObject != null) {
//                            jsonObject.getString("body");
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
//        }
//
//
//
//        @Override
//        public void onMessage(WebSocket webSocket, ByteString bytes) {
//            super.onMessage(webSocket, bytes);
//        }
//
//
//
//        @Override
//        public void onClosing(WebSocket webSocket, int code, String reason) {
//            super.onClosing(webSocket, code, reason);
//        }
//
//
//
//        @Override
//        public void onClosed(WebSocket webSocket, int code, String reason) {
//            super.onClosed(webSocket, code, reason);
//        }
//
//
//
//        @Override
//        public void onFailure(WebSocket webSocket, final Throwable t, @Nullable final Response response) {
//            super.onFailure(webSocket, t, response);
//            Log.d("conectify", "run: no conectado");
//            activity.runOnUiThread(new Runnable() {
//
//                @Override
//                public void run() {
//
//                        try {
//                            Toast.makeText(activity, "Fallo al conectar ", Toast.LENGTH_LONG).show();
//                        }catch (Exception e){
//                            Log.e("tag3", "run: "+e);
//                            Log.d("conectify", "run: no conectado");
//                        }
//
//                }
//
//            });
//        }
//    }





}
