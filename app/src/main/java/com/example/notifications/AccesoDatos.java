package com.example.notifications;

import android.content.Context;
import android.util.Log;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.HashMap;
import java.util.Map;

public abstract class AccesoDatos {

    static String NAMESPACE = "http://regisof.com/";
    //	static String URL = "http://regisofserver.com/Camaras/Servicios.asmx";
   static String URL = "http://192.168.0.46:5555/Camaras/Servicios.asmx";
   // static String URL = "http://192.168.55.225/Camara/Servicios.asmx";	//Palos
//	 static String URL = "http://172.26.0.120/Camaras/Servicios.asmx"; //Cartaya
    static String METHOD_NAME = "";
    static String SOAP_ACTION = "";

    static Map<String, String> parametros = new HashMap<String, String>();
    static Context context;


    public void saveMensaje(String mensaje){



    }

    protected static void setMetodo(String method){
        Log.v("soap", "Llamando a WS " + method);
        METHOD_NAME = method;
        SOAP_ACTION = NAMESPACE + method;
        parametros.clear();
    }

    protected static void agregarParametro(String nombre, String valor){
        Log.v("soap", "Agregando parametro " + nombre + ": " + valor);
        parametros.put(nombre, valor);
    }

    protected static String enviar(){
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

        for(Map.Entry<String, String> parametro: parametros.entrySet()){
            request.addProperty(parametro.getKey(), parametro.getValue());
        }

        SoapSerializationEnvelope envelope =
                new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        String url =  context.getString(R.string.SERVER_URL);






        HttpTransportSE transporte = new HttpTransportSE(url);
        try{
            transporte.call(SOAP_ACTION, envelope);
            SoapPrimitive resultado_xml =(SoapPrimitive)envelope.getResponse();
            String res = resultado_xml.toString();
            Log.v("soap", "Resultado: " + res);
            return res;
        }
        catch (Exception e){
            Log.d("soap", "Error registro en servidor: " + e);
            return "";
        }
    }




    public static void saveInstalacion(String token, int i, String idDevice) {
        setMetodo("InstalacionToken");

        agregarParametro("token", token);
        agregarParametro("oidapp", String.valueOf(i));
        agregarParametro("dispositivo", idDevice);
        SSLConnection.allowAllSSL();
        enviar();
    }

    public static String getMensajes(String fecha) {
        setMetodo("getMensaje");
        agregarParametro("fecha", fecha);

        return enviar();
    }


}
