package com.example.notifications;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = DB.NAME, version = DB.VERSION)
public class DB {
    public static final String NAME = "notify";

    public static final int VERSION = 4;


}
