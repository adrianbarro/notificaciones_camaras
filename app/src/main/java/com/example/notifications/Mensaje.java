package com.example.notifications;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;


@Table(database = DB.class)
public class Mensaje extends BaseModel {

    @Column
    @PrimaryKey(autoincrement=true)

    long id;

    @Column
    String mensaje;

    @Column
    String usuario;

    @Column
    String Date;



    public Mensaje() {
    }

    public Mensaje(long id, String mensaje, String usuario, String date) {
        this.id = id;
        this.mensaje = mensaje;
        this.usuario = usuario;
        this.Date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }
}
