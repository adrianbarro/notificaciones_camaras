package com.example.notifications;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Locale;

import static android.widget.Toast.LENGTH_LONG;


class getMensajesList extends AsyncTask<String, Void, Boolean> {


    private String fecha = "";

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());


    public getMensajesList(String fecha,Context ctx) {
        this.fecha = fecha;
    }

    @Override
    protected void onPreExecute() {
        try{
        MainActivity.d = ProgressDialog.show(MainActivity.ctx, "",
                "Cargando mensajes...", true);}catch(Exception ignored){
            Log.d("tag2", "onPreExecute: error"+ignored);
            Toast.makeText(MainActivity.ctx,"error:"+ignored, LENGTH_LONG).show();
        }
    }

    @Override
    protected Boolean doInBackground(String... params) {

        try {

            JSONArray respuesta = new JSONArray(AccesoDatos.getMensajes(fecha));

            for(int i = 0; i < respuesta.length(); i++) {
                Log.d("TAG3", "Mensaje" + respuesta.getJSONObject(i));
                JSONObject msj = respuesta.getJSONObject(i);
                Mensaje mensaje = new Mensaje();
                mensaje.setUsuario(msj.getString("usuario"));
                mensaje.setMensaje(msj.getString("mensaje"));
                mensaje.setDate(formatfecha(msj.getString("fecha")));
                mensaje.setId(msj.getInt("id"));
                mensaje.save();

            }

            return true;
        } catch (Exception e) {
            Log.d("TAG3", "Error: " + e.getMessage());

            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean arg0) {
      MainActivity.d.dismiss();
       // Toast.makeText(ctx, "La instalacion se realizó correctamente", Toast.LENGTH_SHORT).show();
        /*MainActivity main = new MainActivity();
        main.loadAdapter();*/
    }

    private String formatfecha(String fecha){
        Log.d("fecha", fecha);
        String[] fechamod = fecha.split(" ");
        return fechamod[0];
    }
}
