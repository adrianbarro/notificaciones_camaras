package com.example.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

public class MyReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context ctx, Intent intent) {
        Log.d("backgroundService1","Iniciado");
        ctx.stopService(new Intent(ctx, MyService.class));
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.O)
            ctx.startForegroundService(new Intent(ctx, MyService.class));
        else
            ctx.startService(new Intent(ctx, MyService.class));
    }
}
