package com.example.notifications;

import com.raizlabs.android.dbflow.annotation.Migration;
import com.raizlabs.android.dbflow.sql.SQLiteType;
import com.raizlabs.android.dbflow.sql.migration.AlterTableMigration;

@Migration(version = 4, database = DB.class)
public class Migracion  extends AlterTableMigration<Mensaje> {

    public Migracion(Class<Mensaje> table) {
        super(table);
    }

    @Override
    public void onPreMigrate() {
        super.onPreMigrate();
        addColumn(SQLiteType.TEXT, "Date");
        addColumn(SQLiteType.TEXT, "usuario");
    }
}
