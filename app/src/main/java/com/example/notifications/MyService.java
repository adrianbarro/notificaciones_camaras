package com.example.notifications;

import static android.media.RingtoneManager.getDefaultUri;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

public class MyService extends Service {

    boolean iniciado =false;
    boolean avisado = false;
    public static final String BROADCAST_ACTION = "com.supun.broadcasttest";
    private static final String CHANNEL_ID = "22" ;

    public static Context ctx = null;

    private  NotificationChannel channel;
    private NotificationManagerCompat mNotificationManager;
    Intent intent;

    private Handler handler = new Handler();
    public static WebSocket webSocket;
    BroadcastReceiver broadcast;
    private OkHttpClient client;
    public SocketListener socketListener;
    public int MAX_INTENTOS_RECONEXION = 5000;
    public int intentosReconexion = MAX_INTENTOS_RECONEXION;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.w("conectify", "inicio...");


        createNotificationChannel();
        mNotificationManager = NotificationManagerCompat.from(getApplicationContext());
        intent=new Intent(BROADCAST_ACTION);
        instantiateWebSocket();
        startForeground(-1, buildForegroundNotification(true));


                broadcast = new BroadcastReceiver() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onReceive(Context context, Intent intent) {
                final Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getApplicationContext().getPackageName() + "/" + R.raw.sound);  //Here is FILE_NAME is the name of file that you want to play


                int requestID = (int) System.currentTimeMillis();

                Uri alarmSound = getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


                Intent notificationIntent = new Intent(getApplicationContext(), MyService.class);


                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                Intent resultIntent = new Intent(context, MainActivity.class);

                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                stackBuilder.addNextIntentWithParentStack(resultIntent);

                PendingIntent contentIntent =
                        stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_ac_unit_black_24dp)
                        .setContentTitle("Partes de frio")
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(intent.getStringExtra("body")))
                        .setVibrate(new long[] { 1000, 1000, 1000})
                        .setContentText(intent.getStringExtra("body")).setAutoCancel(true);
                mBuilder.setSound(sound);
                mBuilder.setContentIntent(contentIntent);

                mNotificationManager.notify((int) System.currentTimeMillis(), mBuilder.build());
                mNotificationManager.getNotificationChannel(CHANNEL_ID);

            }
        };
        getApplicationContext().registerReceiver(broadcast, new IntentFilter(MyService.BROADCAST_ACTION));


    }
    @Override
    public void onDestroy() {
        stopForeground(true);
        getApplicationContext().unregisterReceiver(broadcast);
        super.onDestroy();
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }


    public void instantiateWebSocket() {

          /*  if( this.webSocket != null){
            this.webSocket.close(1000,"reconexion");
        }*/


        Log.d("conectify", "instantiateWebSocket: " + webSocket);
        client = new OkHttpClient().newBuilder()
                .readTimeout(2, TimeUnit.SECONDS)
                .connectTimeout(2,TimeUnit.SECONDS)
                .build();


        Request request = new Request.Builder().url(getApplicationContext().getString(R.string.SOCKET_SERVER)).build();

        socketListener = new SocketListener();



        webSocket = client.newWebSocket(request, socketListener);
        client.connectionPool().evictAll();



    }

    private Notification buildForegroundNotification(boolean esNotificada) {

        String socketState = iniciado != false? "CONECTADO" : "DESCONECTADO";
        RemoteViews view = new RemoteViews(getPackageName(),R.layout.disconected_state);


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_ac_unit_black_24dp)
                .setContentTitle("Servicio de notificaciones")
                .setCustomContentView(iniciado != false ? new RemoteViews(getPackageName(),R.layout.conected_state): view)
                .setVibrate(new long[] { 1000, 1000, 1000})
                .setOnlyAlertOnce(esNotificada)// si es true no notifica
                .setColor(Color.GRAY)
                .setContentText(socketState).setAutoCancel(true);
        //mBuilder.setSound(sound);

        return mBuilder.build();
    }



    public class SocketListener extends WebSocketListener {



        Handler handler =new Handler();



        @Override
        public void onOpen(WebSocket webSocket, Response response) {

            Log.d("conectify", "run: conectado");

            handler.post(new Runnable() {

                @Override
                public void run() {
                    try {
                   /*     Toast.makeText(ctx, "Conexion establecida ", Toast.LENGTH_LONG).show();*/
                        iniciado = true;
                        mNotificationManager.notify(-1, buildForegroundNotification(true));
                        avisado = false;
                        intentosReconexion = MAX_INTENTOS_RECONEXION;
                    }catch (Exception e){
                        Log.e("tag3", "run: "+e);
                        Log.d("conectify", "run: conectado");
                    }

                }

            });

        }


        @Override
        public void onMessage(WebSocket webSocket, final String text) {

            handler.post(new Runnable() {
                @Override
                public void run() {

                    Log.d("mensaje2", "run: "+text);
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(text);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {


                        if (jsonObject != null) {

                            intent.putExtra("body",jsonObject.getString("body"));
                            sendBroadcast(intent);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }



        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            super.onClosing(webSocket, code, reason);

            webSocket.close(1000, null);
        }

        @Override
        public void onFailure(WebSocket webSocket, final Throwable t, @Nullable final Response response) {
            super.onFailure(webSocket, t, response);

            handler.post(new Runnable() {

                @Override
                public void run() {

                    try {
                        iniciado = false;
                        Log.e("conectify", "e"+t);
                        Log.d("conectify", "run: no conectado");
                    /*    Toast.makeText(ctx, "Fallo al conectar ", Toast.LENGTH_LONG).show();*/
                        if(!avisado) {
                            mNotificationManager.notify(-1, buildForegroundNotification(true));
                            avisado = true;
                        }
                        if(intentosReconexion == 0) {
                            mNotificationManager.notify(-1, buildForegroundNotification(false));
                        }
                        intentosReconexion = intentosReconexion -1;

                        //Thread.sleep(10000);
                        Request request = new Request.Builder().url(getApplicationContext().getString(R.string.SOCKET_SERVER)).build();
                        MyService.webSocket = client.newWebSocket(request, socketListener);


                    }catch (Exception e){
                        Log.e("tag3", "run: "+e);
                        Log.d("conectify", "run: no conectado");
                    }

                }

            });
        }
    }

    private void createNotificationChannel() {
        Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getApplicationContext().getPackageName() + "/" + R.raw.sound);  //Here is FILE_NAME is the name of file that you want to play

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();

            channel.enableLights(true);
            channel.setLightColor(Color.MAGENTA);
            channel.enableVibration(true);
            channel.setSound(sound, attributes);
            NotificationManager mNotificationManager = getSystemService(NotificationManager.class);
            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(channel);
            }
        }
    }


}