package com.example.notifications;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {


    ArrayList<Mensaje> mDataset;

    public MyAdapter(ArrayList<Mensaje> mDataset) {
        this.mDataset = mDataset;
    }

    @NonNull
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_text_view,parent,false);
        return new MyAdapter.MyViewHolder(view)  ;
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {
        holder.asignarDatos(mDataset.get(position));

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView mensaje;
        TextView user;
        TextView fecha;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mensaje=itemView.findViewById(R.id.info_text);
            user=itemView.findViewById(R.id.user);
            fecha=itemView.findViewById(R.id.date);
        }

        public void asignarDatos(Mensaje dato) {
            Log.d("TAG1", "mensaje " + dato);
            mensaje.setText(dato.getMensaje());
           user.setText(dato.getUsuario());
            fecha.setText(dato.getDate());

        }
    }
}